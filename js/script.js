const app = (() => {
  const ground = 700;
  let posX = 0;
  let posY = ground / 10;
  // shots
  let sPosX = 0;
  let sPosY = 0;
  // velocity shot
  const vXS = 35;
  // mostro pos
  let mPosX = 850;
  let mPosY = 600;
  // mostro frames
  let mfr = 0;
  let spriteMX = 0;
  let spriteMY = 0;
  let frame = 1;
  let characterDIV;
  let shot;
  let fps = 15;
  let animation = 4;
  let movement = 0;
  let isJumping = false;
  let goingUp = true;
  const jumpHeight = 50;
  const keysPressed = {};
  let isShooting = false;
  // mostro HP
  let mostroHP = 100;

  const render = () => {
    // Pintar personaje
    const spriteX = frame * 96.15;
    const spriteY = 104 * animation;
    characterDIV.style.backgroundPositionX = `${spriteX}px`;
    characterDIV.style.backgroundPositionY = `${spriteY}px`;
    characterDIV.style.top = `${posY * 10}px`;
    characterDIV.style.left = `${posX * 10}px`;
    // render shot
    shot.style.top = `${sPosY * 10}px`;
    shot.style.left = `${sPosX * vXS}px`;
    //render mostro
    const spriteMX = mfr * -118;
    mostro.style.top = `${mPosY}px`
    mostro.style.left = `${mPosX}px`;
    mostro.style.backgroundPositionX = `${spriteMX}px`;
  };

  mostroM = () => {
    x = mPosX; // center
    y = mPosY;    // center
    r = 20;   // radius
    a = 0    // angle (from 0 to Math.PI * 2)

    function rotate(a) {
      
      var px = x + r * Math.cos(a); // <-- that's the maths you need
      var py = y + r * Math.sin(a);
      
      mPosX = px;
      mPosY = py;  
    }
    const animOns = setInterval(function() {
      a = (a + Math.PI / fps) % (Math.PI * 2);
      rotate(a);
      if (mostroHP <= 0) {
        clearTimeout(animOns);
      }
    },  1000 / fps);
  }
  monsterDies = () => {
    const animdeath = setInterval(function() {
      mPosX = mPosX + ((Math.random() > 0.5)? -10: +10);
      mPosY += 10;
      if (mPosY >= 681) {
        mfr = 2;
        clearInterval(animdeath);
      }
    },  1000 / fps);
    console.log(mPosY,ground,mostro.offsetHeight,mostro.offsetHeight + mPosY);
  }
  monsterIsHit = ()=> {
    mostroHP -= 34;
    mfr = 1;
    if (mostroHP > 0) {
      setTimeout(() => {
        mfr = 0;
      }, 750);
    } else {
      monsterDies();
    }
  }

  const init = () => {
    console.log("ready");
    characterDIV = document.getElementById("sprite");
    shot = document.getElementById("shot");
    mostro = document.getElementById("mostro");
    render();
    mostroM();
    // mostro
    

    const interval = setInterval(() => {
      // Control
      document.addEventListener("keydown", (ev) => {
        const keyPressed = ev.key;
        console.log(keyPressed);
        switch (keyPressed) {
          case "ArrowUp":
            movement = 1;
            break;
          case "ArrowDown":
            movement = 2;
            break;
          case "ArrowLeft":
            movement = 3;
            break;
          case "ArrowRight":
            movement = 4;
            break;
          case "e":
            movement = 5;
          //keysPressed[shooting] = true;
          break;
          default:
            return;
            break;
        }
        keysPressed[movement] = true;
      });

      document.addEventListener("keyup", (ev) => {
        
        const keyPressed = ev.key;
        let tracking;
        switch (keyPressed) {
          case "ArrowUp":
            tracking = 1;
            break;
          case "ArrowDown":
            tracking = 2;
            break;
          case "ArrowLeft":
            tracking = 3;
            break;
          case "ArrowRight":
            tracking = 4;
            break;
          case "e":
            tracking = 5;
            break;
          default:
            return;
            break;
        }

        delete keysPressed[tracking];
        if (Object.keys(keysPressed).length === 0 && keysPressed.constructor === Object) {
          movement = 0;
        }
      });

      // Update
      shoot = () => {
        shot.style.display = 'block';
        // controls direction of shot
        let leftDir = false;
        if (animation == 3) {
          sPosX = ((posX * 10) / vXS)  - (3/vXS)/*offset*/;
          leftDir = true;
        } else {
          sPosX = ((posX * 10) / vXS) + (characterDIV.offsetWidth/ vXS) - (3/vXS)/*offset*/;
        }
        
        sPosY = posY + ((characterDIV.offsetHeight/2)/10);
        console.log("sp:"+ sPosX,"pos:" + posX, sPosX * 15, posX * 10);
        var timer = setInterval(function() {
          if (leftDir) {
            sPosX--;
          } else {
            sPosX++;
          }
          // shot hits target control/reset
          if ((sPosX* vXS ) >= (mostro.offsetLeft + 10) && (sPosY * 10 ) >= (mostro.offsetTop) && (sPosY * 10 ) <= (mostro.offsetTop + mostro.offsetHeight)) {
            shot.style.display = 'none';
            isShooting = false;
            monsterIsHit();
            clearInterval(timer);
            return;
          }
          // shot out of screen control/reset
          if (((sPosX* vXS ) + 15) >= (window.innerWidth - 20 ) || sPosX <= 0) {
            shot.style.display = 'none';
            isShooting = false;
            clearInterval(timer);
          } 
        }, 1000 / fps);
        return;
      }
      if (keysPressed[5] && !isShooting) {
        isShooting = true;
        shoot();
      }
      if (movement == 0) {
        frame = 0;
      } else {
        jump = () => {
          var timer = setInterval(function() {
            if (posY == (ground/10) && goingUp || (posY * 10) > (ground - jumpHeight ) && goingUp) {
              posY--;
              if ((posY*10) == (ground - jumpHeight)) {goingUp = false;}
            } else if (posY < (ground/10)) {
              posY++;
              if((posY*10) == (ground)){goingUp = true;clearInterval(timer);isJumping = false;}
            }
          }, 1000 / fps);
          return;
        }
        if (keysPressed[1]) {
          if(!isJumping) { 
            isJumping = true;
            jump();
          }
        }
        if (keysPressed[3]) {
          animation = 3;
          posX--;
        } 
        if (keysPressed[4]) {
          animation = 1;
          posX++;
        }
        frame++;
      }
      render();
    }, 1000 / fps);
  };
  return { init };
})();

document.addEventListener("DOMContentLoaded", app.init);
